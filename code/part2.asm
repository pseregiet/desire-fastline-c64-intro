// FASTLINE BBS INTRO BY DESIRE
// CODE BY GOLARA IN 2019
// https://gitlab.com/pseregiet/desire-fastline-c64-intro

part2_irq:
            sta regap2
            lda last_d011:#00
            cmp #6
            beq !+
            jsr waster2
            nop
            nop
            nop
!:
            lda part2_color0:#$00 // 03
            sta $d020
            stx regxp2
            sty regyp2
            lda last_d011
            cmp #7
            beq !+
            jsr waster3
!:
            jsr waster3
            jsr waster3
            jsr waster1
            nop
            lda part2_color1:#$00 // 0e
            sta $d020
            jsr muzak.play
            jsr drive_the_text


            lda #<part2_top_irq
            sta $fffe
            lda #>part2_top_irq
            sta $ffff
            lda #1
            sta $d012

            lda regap2:#00
            ldx regxp2:#00
            ldy regyp2:#00

            inc $d019
            rti

part2fadeoutirq:
            sta pp2a
            stx pp2x
            sty pp2y
            jsr muzak.play
            lda pp2a:#00
            ldx pp2x:#00
            ldy pp2y:#00
            inc $d019
            rti

waste_frames:
            ldx #50
wasteloop:
            lda #250
!:          cmp $d012
            bne !-
            lda #50
!:          cmp $d012
            bne !-
            dex
            bne wasteloop
            rts

part2_transition_to_part1:
            lda #$00
            sta $d011

            lda #<part2fadeoutirq
            sta $fffe
            lda #>part2fadeoutirq
            sta $ffff
            inc $d019
            cli

            jsr setup_introbar_sprites
            jsr copy_part1bitmap_back
            jsr waste_frames
            ldx #$01
            stx $d027
            stx $d028
            stx $d029
            dex
            stx $d01c

            lda #<irq_intro
            sta $fffe
            lda #>irq_intro
            sta $ffff
            lda #$ff
            sta $d01d
            lda #8
            sta bar1_
            sta bar1
            lda #16
            sta bar2
            lda #$01
            sta disable_skater
            lda #$00
            sta text_timer
            
            //sta effect1_time
            sta bar3
            sta bcis
            sta bci
            sta basic1
            sta basic2
            lda #$18
            sta basic3
            lda #$fe
            sta effect2_time
            rts
            

part2_top_irq:
            sta regap21
            stx regxp21
            sty regyp21

            lda p2c3idx
            cmp #8
            bne !+
            lda pal6
            cmp #<colorfade_05+8
            bne !+
            jsr part2_transition_to_part1
            jmp part2topend
!:
            lda #$01
            sta $dd00

            lda #$12
            sta $d018
            lda #$18
            sta $d016
            lda #$1b
            sta $d011


            lda part2_color5:#00 //4
            sta $d022
            lda part2_color6:#00 //7
            sta $d023
            lda part2_color7:#04
            sta $d025
            lda part2_color8:#07
            sta $d026
            lda part2_color9:#07
            sta $d027
            sta $d028
            sta $d029

            lda part2_color3:#$00 // 06
            sta $d020
            sta $d021

            lda #$7
            sta $d015
            sta $d01c

            lda #$00
            sta $d01d
            sta $d017
            ldx #(sprite_B / $40)
            stx $87f8 + 0
            stx $87f8 + 1
            inx
            stx $87f8 + 2

            ldx bbsy1:#00
            lda bbsy, x
            sta $d001
            ldx bbsy2:#10
            lda bbsy, x
            sta $d003
            ldx bbsy3:#20
            lda bbsy, x
            sta $d005

            ldx bbsx1:#00
            lda bbsx, x
            sta $d000
            ldx bbsx2:#10
            lda bbsx, x
            sta $d002
            ldx bbsx3:#20
            lda bbsx, x
            sta $d004
            lda #$7f

            inc bbsx1
            inc bbsx2
            inc bbsx3
            inc bbsy1
            inc bbsy2
            inc bbsy3

/*
            ldx bbsy1
            axs #1
            stx bbsy1
            ldx bbsy2
            axs #1
            stx bbsy2
            ldx bbsy3
            axs #1
            stx bbsy3

            ldx bbsx1
            axs #-1
            stx bbsx1
            ldx bbsx2
            axs #-1
            stx bbsx2
            ldx bbsx3
            axs #-1
            stx bbsx3
*/          
            jsr part2_fadein
            lda xxx:#00
            and #0
            bne !+
            jsr scroll_cable_colors
!:
            inc xxx
        
            ldx #$10
            ldy #$08

            lda #99
!:          cmp $d012
            bne !-
            stx $d018
            sty $d016
            ldx fld_idx:#127
            lda fldtab, x
            jsr dofld
            nop
            nop
            nop
            jsr waster1
            lda part2_color4:#00
            sta $d020
            sta $d021

            ldx fld_idx
            dex
            lda fldtab, x
            cmp #$30
            bcs dont_change_bottom_color
            clc
            adc #$c5
!:          cmp $d012
            bne !-
            jsr waster3
            nop
            nop
            lda part2_color3
            sta $d020
            sta $d021
dont_change_bottom_color:
            lda disable_skater:#1
            bne !+
            jsr setup_skater
!:            
            //lda #$00
            //sta $d015

            lda #<part2_irq
            sta $fffe
            lda #>part2_irq
            sta $ffff
            lda #246
            sta $d012
part2topend:
            lda regap21:#00
            ldx regxp21:#00
            ldy regyp21:#00

            inc $d019
            rti

drive_the_text:
            ldx text_timer:#$b0
            inx
            bne dont_change_text
//time to change the text. start fld
            ldy #$01
            sty disable_skater
            ldy fld_idx
            iny
            beq fld_done
            cpy #128
            beq text_is_down
            sty fld_idx
            rts
dont_change_text:
            stx text_timer
            rts

fld_done:
            lda #$00
            sta text_timer
            sta fld_idx
            sta disable_skater
            //sta skater_gox
            rts
text_is_down:
            lda #$00
            sta $d000 + (4*2)
            sta $d000 + (5*2)
            sta $d000 + (6*2)
            sta $d000 + (7*2)
            sta $d010
            //replace text
            ldx pages_shown
            inx
            cpx #8*3
            bne !+
            jsr part2_fadeout
!:
            stx pages_shown
            lda #$01
            sta change_textpage
            lda textpage_replaced
            beq !+
            sty fld_idx
            lda #$00
            sta change_textpage
            sta textpage_replaced
!:
            rts

part2_fadein:
            inc p2skip
            lda p2skip:#00
            and #3
            bne p2end
            ldx p2c0idx:#00
            cpx #8
            beq !+
            lda pal1:colorfade_03, x
            sta part2_color0
            inc p2c0idx
            bne p2end
!:
            ldx p2c1idx:#00
            cpx #8
            beq !+
            lda pal2:colorfade_0e, x
            sta part2_color1
            inc p2c1idx
            bne p2end
!:
            ldx p2c2idx:#00
            cpx #8
            beq !+
            lda pal3:colorfade_06, x
            sta part2_color3
            lda pal4:colorfade_04, x
            sta part2_color5
            lda pal5:colorfade_07, x
            sta part2_color6
            inc p2c2idx
            bne p2end
!:
            ldx p2c3idx:#00
            cpx #8
            beq p2end
            lda pal6:colorfade_05, x
            sta part2_color4
            inc p2c3idx
p2end:
            rts

write_string:
            // text1 & 2 set to string ptr + 39
            // $04/$05 loaded with ptr to screen
            ldy #39
!:
            lda text1:$ffff, y
            sta ($04), y
            dey
            bpl !-
            ldy #39
            clc
!:
            lda text2:$ffff, y
            adc #$40
            sta ($06), y
            dey
            bpl !-
            rts
next_string:
            jsr write_string
            clc
            lda $04
            adc #120
            sta $04
            bcc !+
            inc $05
            clc
!:
            lda $06
            adc #120
            sta $06
            bcc !+
            inc $07
            clc
!:
            lda text1
            adc #40
            sta text1
            sta text2
            bcc !+
            inc text1+1
            inc text2+1
!:
            lda $04
            cmp #<($8400 + 280 + (120*4))
            bne !+
            lda $05
            cmp #>($8400 + 280 + (120*4))
            bne !+

            lda #<($8400 + 280)
            sta $04
            lda #>($8400 + 280)
            sta $05
            lda #<($8400 + 320)
            sta $06
            lda #>($8400 + 320)
            sta $07
!:
            rts

dofld:
            tax
            clc
            beq skipfld
fldloop:
            lda $d012
            adc #1
!:          cmp $d012
            bne !-
            adc #5
            and #7
            sta last_d011
            ora #$10
            sta $d011
            dex
            bne fldloop
skipfld:
            rts

part2_outofirqloop:
            lda change_textpage:#00
            beq part2_outofirqloop
            jsr next_string
            jsr next_string
            jsr next_string
            jsr next_string
            lda #$01
            sta textpage_replaced
            lda #$00
            sta change_textpage
            jmp part2_outofirqloop
textpage_replaced: .byte $00
pages_shown: .byte $00

part2_fadeout:
            lda #$ff
            lax pal1
            axs #-8
            stx pal1
            lax pal2
            axs #-8
            stx pal2
            lax pal3
            axs #-8
            stx pal3
            lax pal4
            axs #-8
            stx pal4
            lax pal5
            axs #-8
            stx pal5
            lax pal6
            axs #-8
            stx pal6

            ldx #$00
            stx p2c0idx
            stx p2c1idx
            stx p2c2idx
            stx p2c3idx
            //sta pages_shown
            rts

copy_part1bitmap_back:
            ldx #$00
!:
            .for (var i = 0; i < $10; i++)
            {
                lda $a000 + i*$100,x
                sta $4000 + i*$100,x
            }
            inx
            bne !-
!:
            .for (var i = $10; i < $20; i++)
            {
                lda $a000 + i*$100,x
                sta $4000 + i*$100,x
            }
            inx
            bne !-
            rts

scroll_cable_colors:
            ldx #38
!:
            lda $d800, x
            sta $d800 + 00*40 + 1, x
            sta $d800 + 01*40 + 1, x
            sta $d800 + 02*40 + 1, x
            sta $d800 + 03*40 + 1, x
            sta $d800 + 04*40 + 1, x
            sta $d800 + 05*40 + 1, x
            dex
            bpl !-
            ldx scci:#00
            lda cables_colors, x
            sta $d800 + 00*40
            sta $d800 + 01*40
            sta $d800 + 02*40
            sta $d800 + 03*40
            sta $d800 + 04*40
            sta $d800 + 05*40
            lda #$7f
            axs #-1
            stx scci
            rts



.align $100
scrolltext:
* = * "pages"
.encoding "screencode_upper"
.text "             DESIRE IS BACK             "
.text "  AT FOREVER 2019 WITH THIS BBSTRO FOR  "
.text "                                        "
.text "            F A S T L I N E             "

.text "  FASTLINE IS THE COOPERATION BBS FOR   "
.text " HOKUTO-FORCE MAYDAY LAXITY TRSI DESIRE "
.text "         YOU SHOULD CONNECT VIA         "
.text "         FASTLINE.NU  PORT 1541         "

.text "      THE DEVELOPMENT TEAM CREDITS      "
.text "SOFTWARE BY GOLARA   SOUNDTRACK BY NO-XS"
.text "GLYPHS BY BOKANOID   COACHING BY RAMONB5"
.text " AND PIXEL VISUALIZATION BY PEAKREACHER "

.text " CALL FASTLINE FOR YOUR   AMIGA   ATARI "
.text " SPECTRUM    PC    PLUS    C64    STUFF "  
.text " AND MEET   THE FIX  OUR FRIENDLY SYSOP "
.text "    CALL   FASTLINE.NU    PORT 1541     "

.text "       DESIRE SENDS GREETINGS TO:       "
.text " ALCATRAZ ALTAIR ARSENIC ARISE ATLANTIS "
.text "  ATE-BIT BOOZE BAUKNECHT CRTC CENSOR   "
.text " CHORUS CAMELOT EXCESS EXTEND DARKLITE  "

.text "   DEADLINERS FOCUS-DESGIN G-P HORNET   "
.text "FAIRLIGHT INFECT INSANE JAC! LAXITY F4CG"
.text "MAYDAY! OXYRON THE-DREAMS PVM RIFT SAMAR"
.text "   TRIAD TEMPEST RESOURCE HORNET TRSI   "

.text "  TEMPEST LOW-SPIRIT RESISTANCE TITAN   "
.text " QUEBARIUM  SENSENSTAHL AND  NAH-KOLOR  "
.text "                                        "
.text "    EVERYONE AT FOREVER 2019 PARTY !    "

.text "                                        "
.text "                                        "
.text "                                        "
.text "                                        "


